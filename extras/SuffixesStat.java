import java.io.*;
import java.util.*;

class SuffixesStat{


	public static void main(String[] args) throws Exception {
		String [] words = new String[78472];
		String [] suffixes = new String[99];
		int i = 0;
		Scanner scanner = new Scanner(new FileReader("StemDictionaryModified(copy).txt"));
		while(scanner.hasNext()){
			String [] word = scanner.nextLine().split(" ");
			if(word.length == 2)
				words[i++] = word[0];

		}
		int j = 0;

		scanner = new Scanner(new FileReader("suff(copy).txt"));
		while(scanner.hasNext()){
			String  word2 = scanner.nextLine();
			word2 = word2.trim();
			suffixes[j++] = word2;

		}
		System.out.println(j);
		Arrays.sort(suffixes, new java.util.Comparator<String>() {
    @Override
    public int compare(String s1, String s2) {
        // TODO: Argument validation (nullity, length)
        return s2.length() - s1.length();// comparision
    }
});
		

		//HashMap
		HashMap<String,Integer> map = new HashMap<String,Integer>();
		for (int k = 0;k<j;k++){
			map.put(suffixes[k],0);
		}

		for (int k = 0;k<i;k++){
			for (int l = 0;l<j;l++){
				if(words[k].contains(suffixes[l])){
					int count = map.get(suffixes[l]);
					map.put(suffixes[l],++count);
					break;
				}
			}
		}

		map = sortByValue(map);


		//for writing to file
		PrintWriter printWriter = new PrintWriter(new File("suffix_count_stats.txt"));

		for(String entry : map.keySet()){
			System.out.println(entry +" "+map.get(entry));
			printWriter.println(entry +" "+map.get(entry));
		}
		printWriter.close();


	}
   private static HashMap<String, Integer> sortByValue(Map<String, Integer> unsortMap) {

        // 1. Convert Map to List of Map
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        HashMap<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        /*
        //classic iterator example
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext(); ) {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }*/


        return sortedMap;
    }
}